
const express = require('express');
const path = require('path');
const appInit = require('./service/init');
const router = require('./service/router');
const app = express();
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(express.static(path.join(__dirname, 'public')));
appInit.exe(app);
router.route(app);
module.exports = app;
