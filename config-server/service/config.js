const utils = require('../util/common');
const configsDao = require('../db/configs');
const logger = require('../util/logger');
const configSev = () => { };
configSev.getConfig = async (params) => {
    logger.log('[configSev.getConfig] start : params ' + JSON.stringify(params));
    const result = {};
    if (params.length !== 1) {
        result.code = 400;
        result.msg = { error: 'params length is not correct' };
        return result;
    }
    const config = {};
    config.name = params[0];
    try {
        const configResp = await configsDao.findOne({ name: config.name });
        result.code = 200;
        result.msg = { state: 'success', config: configResp.config };
        return result;

    } catch (error) {
        result.code = 500;
        result.msg = error;
        return result;
    }
}
configSev.upsert = async (params) => {
    logger.log('[configSev.upsert] start : params ' + JSON.stringify(params));
    const result = {};
    if (params.length !== 3) {
        result.code = 400;
        result.msg = { error: 'params length is not correct' };
        return result;
    }    
    const config = {};
    config.config_id = utils.md5(params[0]);
    config.name = params[1];
    config.config = params[2];
    //params[2];
    try {
        const configResp = await configsDao.findOne({ name: config.name });
        if (configResp) {
            await configsDao.update({ name: config.name }, { config: config.config })
        } else {
            await configsDao.add([config]);
        }
        result.code = 200;
        result.msg = { state: 'success' };
        return result;
    } catch (error) {
        result.code = 500;
        result.msg = error;
        return result;
    }
};
module.exports = configSev;