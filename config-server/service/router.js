
const publicRouter = require('../routes/public');
const secureRouter = require('../routes/secure');
const jwtUtil = require('../util/jwt');
const proxy = require('../util/proxy');
const router = () => { };
router.route = (app) => {
    app.use('/public', publicRouter);
    app.use('/secure', jwtUtil.check, secureRouter);
    app.use('/proxy',jwtUtil.check, proxy.process);
}
module.exports = router;