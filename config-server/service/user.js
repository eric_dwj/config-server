const utils = require('../util/common');
const jwtUtils = require('../util/jwt');
const usersDao = require('../db/users');
const logger = require('../util/logger');
const userUtil = () => { };
userUtil.login = async (params) => {
    logger.log('[userUtil.login] start : params ' + JSON.stringify(params));
    const result = {};
    if (params.length !== 3) {
        result.code = 400;
        result.msg = { error: 'params length is not correct' };
        return result;
    }
    const user = {};
    user.userid = params[0];
    user.email = params[1];
    user.password = params[2];

    if (!utils.validateEmail(user.email)) {
        result.code = 400;
        result.msg = { email: 'email format error' };
        return result;
    }

    user.password = await utils.md5(params[2]);
    try {
        const userResp = await usersDao.findOne(user);
        if (userResp) {
            result.code = 200;
            result.msg = { state: 'success', token: jwtUtils.issue({ userid: user.userid, role: userResp.role }) };
            return result;
        } else {
            result.code = 401;
            result.msg = { state: 'password not correct' };
            return result;
        }

    } catch (error) {
        result.code = 500;
        result.msg = error;
        return result;
    }
};
userUtil.register = async (params) => {
    logger.log('[userUtil.register] start : params ' + JSON.stringify(params));
    const result = {};
    if (params.length !== 5) {
        result.code = 400;
        result.msg = { error: 'params length is not correct' };
        return result;
    }
    const user = {};
    user.userid = params[0];
    user.email = params[1];
    user.password = params[2];
    const repeatPassword = params[3];
    user.role = params[4];
    if (user.password != repeatPassword) {
        result.code = 400;
        result.msg = { error: 'password not equal repeat password' };
        return result;
    }

    if (!utils.validateEmail(user.email)) {
        result.code = 400;
        result.msg = { email: 'email format error' };
        return result;
    }

    user.password = await utils.md5(params[3]);

    try {
        await usersDao.add([user]);
        result.code = 200;
        result.msg = { state: 'success' };
        return result;
    } catch (error) {
        result.code = 500;
        result.msg = error;
        return result;
    }
};
module.exports = userUtil;