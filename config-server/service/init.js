const logger = require('../util/logger');
const connection = require('../db/connection');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const cors = require("cors");
const init = () => { };

const corsOptions = {
    origin: '*',
    optionsSuccessStatus: 200,
}
init.exe = (app) => {
    logger.log('[init.exe] started')
    logger.initAccessLog(app);
    app.use(cookieParser());
    app.use(bodyParser.json({ limit: '50mb' }));
    app.use(bodyParser.urlencoded({ limit: '50mb', extended: true }));
    app.use(cors(corsOptions));
    connection.connectDb(()=>{
        logger.log('[init.exe] DB connected, start loading data');
    });
}

module.exports = init;