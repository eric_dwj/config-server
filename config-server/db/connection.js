
const config = require('../config');
const { mongoose } = require('./schema');
let connected = false;
exports.connectDb = (cb) => {
    process.nextTick(() => {
        if (connected) {
            cb();
        } else {
            mongoose.connect(config.dbcon, { useCreateIndex: true, useNewUrlParser: true })
                .then(() => {
                    connected = true;
                    cb(null, 'success connect to db.');
                })
                .catch((err) => {
                    connected = false;
                    cb(err);
                });
        }
    })
}