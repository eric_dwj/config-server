const { configs_m } = require('./schema');
exports.add = (datas) => {
    return new Promise(async (resolve, reject) => {
        try {
            configs_m.insertMany(datas, (err, records) => ((err) ? reject(err) : resolve(records)));
        } catch (err) {
            reject(err);
        }
    });
}

exports.update = (condition, update) => {
    return new Promise(async (resolve, reject) => {
        try {
            configs_m.updateOne(condition, update, (err, record) => ((err) ? reject(err) : resolve(record)));
        } catch (err) {
            reject(err);
        }
    });
}

exports.findOne = (condition) => {
    return new Promise(async (resolve, reject) => {
        try {
            configs_m.findOne(condition, (err, record) => ((err) ? reject(err) : resolve(record)));
        } catch (err) {
            reject(err);
        }
    });
}

exports.find = (condition) => {
    return new Promise(async (resolve, reject) => {
        try {
            configs_m.find(condition, (err, records) => ((err) ? reject(err) : resolve(records)));
        } catch (err) {
            reject(err);
        }
    });
}