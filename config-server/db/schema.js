const mongoose = require("mongoose");

const configs_s = mongoose.Schema({
    config_id: {
        type: String,
        unique: true,
        required: true,
    },
    name: {
        type: String,
        unique: true,
        required: true,
    },
    config: {
        type: String,
        unique: true,
        required: true,
    },
});

const users_s = mongoose.Schema({
    userid: {
        type: String,
        unique: true,
        required: true,
    },
    email: String,
    password: {
        type: String,
        required: true,
    },
    role: {
        type: String,
        required: true,
        validate: /middleserver|cms/ 
    },
});

const users_m = mongoose.model('users', users_s);
const configs_m = mongoose.model('configs', configs_s);
module.exports = {
    mongoose,
    configs_m,
    users_m,
}