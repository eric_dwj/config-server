const path = require('path');
const fs = require('fs')
const morgan = require('morgan');
const tools = require('../util/common');
const rfs = require('rotating-file-stream');
const log4js = require('log4js');
const config = require('../config');

const loggerUtil = () => { };

const logDirectory = path.join(__dirname, '../../log');
fs.existsSync(logDirectory) || fs.mkdirSync(logDirectory);
const accessLogStream = rfs('access.log', { interval: '1d', path: logDirectory });
log4js.configure({
    appenders: {
        everything: { type: 'dateFile', filename: logDirectory + '/info.log', pattern: '.yyyy-MM-dd', compress: true }
    },
    categories: {
        default: { appenders: ['everything'], level: 'info' }
    }
});

const logger = log4js.getLogger('dateFile');

loggerUtil.log = (msg) => {
    if (config.env === 'dev') {
        console.log('['+tools.getFormatDate(new Date())+'] ' + msg + '\n\r');
    }
    logger.info('['+tools.getFormatDate(new Date())+'] ' + msg + '\n\r');
};

loggerUtil.initAccessLog = (app) => {
    app.use(morgan('combined', { stream: accessLogStream }));
    app.use((req, res, next) => {
        const logMsg = {};
        logMsg.url = req.url;
        logMsg.body = req.body;
        logMsg.headers = req.headers;
        logMsg.params = req.params;
        logMsg.query = req.query;
        logger.info('['+tools.getFormatDate(new Date())+'] [loggerUtil.initAccessLog] ' + JSON.stringify(logMsg, null, 0) + '\n\r');
        if (config.env === 'dev') {
            console.log('['+tools.getFormatDate(new Date())+'] [loggerUtil.initAccessLog] ' + JSON.stringify(logMsg, null, 0) + '\n\r')
        }
        next();
    });
};

module.exports = loggerUtil;