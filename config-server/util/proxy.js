const request = require('request').defaults({ 'proxy': 'http://proxy.jp.sbibits.com:8080' });
// const request = require('request');
const proxUtil = () => { };
const config = require('../config');
const twofa = require('../util/2fa');
proxUtil.process = (req, res, next) => {
    if (req.method === 'POST') {
        const options = {
            url: config.proxy + req.url,
            method: 'POST',
            headers: {
                'otp': twofa.twofaGetOTPToken(config.otp_key)
            },
            json: req.body
        };
        request.post(options, (err, resp, body) => {
            if (err) {
                res.send(err)
            } else {
                if(typeof body == 'string'){
                    res.send(JSON.parse(body));
                }else {
                    res.send(body);
                }
            }
        });
    } else if (req.method === 'GET') {
        const options = {
            url: config.proxy + req.url,
            method: 'GET',
            headers: {
                'otp': twofa.twofaGetOTPToken(config.otp_key)
            }
        };
        request.get(options, (err, resp, body) => {
            if (err) {
                res.send(err)
            } else {
                if(typeof body == 'string'){
                    res.send(JSON.parse(body));
                }else {
                    res.send(body);
                }
                
            }
        });
    }
}
module.exports = proxUtil;