const QRCode = require('qrcode');
const scheduler = require('node-schedule');
const CryptoJS = require("crypto-js");
const md5 = require('md5');
const CommonUtil = () => { };
CommonUtil.validateEmail = (email) => {
    const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
}

CommonUtil.getFormatDate = (date) => {
    const seperator1 = "-";
    const year = date.getFullYear();
    let month = date.getMonth() + 1;
    let strDate = date.getDate();
    if (month >= 1 && month <= 9) {
        month = "0" + month;
    }
    if (strDate >= 0 && strDate <= 9) {
        strDate = "0" + strDate;
    }
    const timestr = date.getHours() + ':' + date.getMinutes()+ ':' + date.getSeconds();
    const FormatDate = year + seperator1 + month + seperator1 + strDate+' ' +timestr;
    return FormatDate;
}

CommonUtil.sleep = (ms) => {
    return new Promise(resolve => {
        setTimeout(resolve, ms)
    })
}

CommonUtil.qrcode = (data) => new Promise((resolve, reject) => {
    QRCode.toDataURL(data, (err, url) => {
        resolve(url);
    });
});

CommonUtil.scheduler = (cron, callback) => {
    scheduler.scheduleJob(cron, callback);
}

CommonUtil.encrypt = (data, password) => new Promise((resolve, reject) => {
    let encryptedkeystore;
    try {
        encryptedkeystore = CryptoJS.AES.encrypt(data, password);
    } catch (exception) {
        reject(exception);
    }
    resolve(encryptedkeystore);
});

CommonUtil.md5 = (message) => md5(message);
CommonUtil.decrypt = (data, password) => new Promise((resolve, reject) => {
    let keyStore;
    try {
        const bytes = CryptoJS.AES.decrypt(data, password);
        keyStore = bytes.toString(CryptoJS.enc.Utf8);
    } catch (exception) {
        reject(exception);
    }
    resolve(keyStore);
});
module.exports = CommonUtil;
