const jwt = require('jsonwebtoken');
const md5 = require('md5');
const tokenSecret = md5('Trader_V1');
const config = require('../config')
const logger = require('../util/logger');

const jwtUtil = () => { };

jwtUtil.check = (req, res, next) => {
    let token;
    if (req.headers && req.headers.authorization) {
        const parts = req.headers.authorization.split(' ');
        
        if (parts.length === 2) {
            const scheme = parts[0], credentials = parts[1];
            if (/^Bearer$/i.test(scheme)) {
                token = credentials;
            }
        } else {
            return res.status(401).json({ err: 'Format is Authorization: Bearer [token]' });
        }
    } else if (req.params.token) {
        token = req.params.token;
        delete req.params.token;
    } else {
        return res.status(401).json({ err: 'No Authorization header was found' });
    }
    jwtUtil.verify(token).then(user => {
        if (user) {
            req.user = user;
            next();
        } else {
            res.status(401).json({ err: 'Invalid Token!' });
        }
    })

};

jwtUtil.issue = (payload) => {
    logger.log('[jwtUtil.issue] start : ' + JSON.stringify(payload))
    const token = jwt.sign(payload, tokenSecret, { expiresIn: config.expire_time });
    return token;
};

jwtUtil.verify = (token) => {
    logger.log('[jwtUtil.verify] start :  token ***' + String(token).substring(token.length-10,token.length))
    return new Promise(resolve => {
        try {
            const decode = jwt.verify(token, tokenSecret);
            resolve(decode);
        } catch (error) {
            logger.log('[jwtUtil.issue] verify not successfull : ' + JSON.stringify(error))
            resolve(false);
        }
    });
}
module.exports = jwtUtil;