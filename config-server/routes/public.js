const express = require('express');
const router = express.Router();
const userSev = require('../service/user');
const configSev = require('../service/config');
router.post('/', async (req, res, next) => {
  const method = req.body.method;
  const params = req.body.params;
  let result;
  if (method === 'register') {
    result = await userSev.register(params);
  } else if (method === 'login') {
    result = await userSev.login(params);
  } else if (method === 'getConfig'){
    result = await configSev.getConfig(params);
  }
  if(result){
    res.status(result.code).json(result.msg);
  }
});

module.exports = router;
