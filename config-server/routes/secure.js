const express = require('express');
const router = express.Router();
const configSev = require('../service/config');
router.post('/', async (req, res, next) => {
  const method = req.body.method;
  const params = req.body.params;
  let result;
  if (method === 'upsertConfig') {
    if (req.user && req.user.role === 'cms') {
      result = await configSev.upsert(params);
    } else {
      result.code = 401;
      result.msg = { error: 'role should be cms' };
    }
  }
  if (result) {
    res.status(result.code).json(result.msg);
  }
});

module.exports = router;
